import java.io.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class main {

    public static void main(String[] args) {

        Document doc = null;
        Elements elem_all = null;

        try {
            doc = Jsoup.connect("http://yandex.ru/yandsearch?text=%D0%9C%D0%B0%D0%B9%D1%81%D0%BA%D0%B0%D1%8F+%D1%82%D1%80%D0%B0%D0%B2%D0%BA%D0%B0+%D0%B3%D0%BE%D0%BB%D0%BE%D0%B4%D0%BD%D0%BE%D0%B3%D0%BE+%D0%BA%D0%BE%D1%80%D0%BC%D0%B8%D1%82&nl=1&lr=2").get();

            elem_all = doc.select("div.main__content div.content__left ul.serp-list li.serp-item");
        } catch (IOException e) {
            e.printStackTrace();
        }

        String text = "";
        int num = 0;
        String filename = "document";
        for (Element elem: elem_all) {
            try(FileWriter writer = new FileWriter(filename + num + ".txt", false))
            {
                writer.write("Header : " + elem.select("h2 a").text()
                        + System.getProperty( "line.separator" ));
                writer.write("Link : " + elem.select("h2 a").attr("href")
                        + System.getProperty( "line.separator" ));
                writer.write("Green line : " + elem.select("div.organic__path").text()
                        + System.getProperty( "line.separator" ));
                writer.write("Description : " + elem.select("span.extended-text__short").text()
                        + System.getProperty( "line.separator" ));
                writer.flush();
            }
            catch(IOException ex){
                System.out.println(ex.getMessage());
            }
            num++;
        }
    }
}
